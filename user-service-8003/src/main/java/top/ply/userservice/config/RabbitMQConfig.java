package top.ply.userservice.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Value("${spring.rabbitmq.verify-code-exchange.name}")
    public String verifyCodeExchangeName;

    @Value("${spring.rabbitmq.email-verify-code-queue.name}")
    public String emailVerifyCodeQueueName;

    @Value("${spring.rabbitmq.email-verify-code-queue.routingKey}")
    public String emailVerifyCodeQueueRoutingKey;

    @Value("${spring.rabbitmq.phone-verify-code-queue.name}")
    public String phoneVerifyCodeQueueName;

    @Value("${spring.rabbitmq.phone-verify-code-queue.routingKey}")
    public String phoneVerifyCodeQueueRoutingKey;

    @Bean("${spring.rabbitmq.verify-code-exchange.name}")
    public Exchange verifyCodeExchange() {
        return new TopicExchange(verifyCodeExchangeName, false, true);
    }

    @Bean("${spring.rabbitmq.email-verify-code-queue.name}")
    public Queue emailVerifyCodeQueue() {
        return new Queue(emailVerifyCodeQueueName, false, false, true);
    }

    @Bean
    public Binding emailVerifyCodeQueueBinding(@Qualifier("${spring.rabbitmq.verify-code-exchange.name}") Exchange verifyCodeExchange,
                                               @Qualifier("${spring.rabbitmq.email-verify-code-queue.name}") Queue emailVerifyCodeQueue) {
        return BindingBuilder
                .bind(emailVerifyCodeQueue)
                .to(verifyCodeExchange)
                .with(emailVerifyCodeQueueRoutingKey)
                .noargs();
    }

    @Bean("${spring.rabbitmq.phone-verify-code-queue.name}")
    public Queue phoneVerifyCodeQueue() {
        return new Queue(phoneVerifyCodeQueueName, false, false, true);
    }

    @Bean
    public Binding phoneVerifyCodeQueueBinding(@Qualifier("${spring.rabbitmq.verify-code-exchange.name}") Exchange verifyCodeExchange,
                                               @Qualifier("${spring.rabbitmq.phone-verify-code-queue.name}") Queue phoneVerifyCodeQueue) {
        return BindingBuilder
                .bind(phoneVerifyCodeQueue)
                .to(verifyCodeExchange)
                .with(phoneVerifyCodeQueueRoutingKey)
                .noargs();
    }

}
