package top.ply.userservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class RedisTemplateCustomConfig {

    @Value("${spring.redis.telephoneCode.host}")
    private String telephoneCodeHost;

    @Value("${spring.redis.telephoneCode.port}")
    private int telephoneCodePort;

    @Value("${spring.redis.telephoneCode.db}")
    private int telephoneCodeDB;

    @Value("${spring.redis.telephoneCode.pass}")
    private String telephoneCodeDBPass;

    @Value("${spring.redis.emailCode.host}")
    private String emailCodeHost;

    @Value("${spring.redis.emailCode.port}")
    private int emailCodePort;

    @Value("${spring.redis.emailCode.db}")
    private int emailCodeDB;

    @Value("${spring.redis.emailCode.pass}")
    private String emailCodeDBPass;

    @Value("${spring.redis.updatePhoneCode.host}")
    private String updatePhoneCodeHost;

    @Value("${spring.redis.updatePhoneCode.port}")
    private int updatePhoneCodePort;

    @Value("${spring.redis.updatePhoneCode.db}")
    private int updatePhoneCodeDB;

    @Value("${spring.redis.updatePhoneCode.pass}")
    private String updatePhoneCodePass;

    @Value("${spring.redis.updateEmailCode.host}")
    private String updateEmailCodeHost;

    @Value("${spring.redis.updateEmailCode.port}")
    private int updateEmailCodePort;

    @Value("${spring.redis.updateEmailCode.db}")
    private int updateEmailCodeDB;

    @Value("${spring.redis.updateEmailCode.pass}")
    private String updateEmailCodePass;

    @Value("${spring.redis.max-idle}")
    private int maxIdle;

    @Value("${spring.redis.max-total}")
    private int maxTotal;

    @Value("${spring.redis.max-wait-millis}")
    private long maxWaitMillis;

    @Value("${spring.redis.testOnBorrow?:true}")
    private boolean testOnBorrow;

    public JedisPoolConfig jedisPoolConfig(int maxIdle, int maxTotal, long maxWaitMillis, boolean testOnBorrow) {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxIdle(maxIdle);
        config.setMaxTotal(maxTotal);
        config.setMaxWaitMillis(maxWaitMillis);
        config.setTestOnBorrow(testOnBorrow);
        return config;
    }

    public RedisConnectionFactory redisConnectionFactory(String host, int port, String password, int maxIdle,
                                                         int maxTotal, long maxWaitMillis, int index) {
        RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
        config.setHostName(host);
        config.setPort(port);
        config.setDatabase(index);
        config.setPassword(RedisPassword.of(password));
        JedisClientConfiguration.JedisPoolingClientConfigurationBuilder jpcb =
                (JedisClientConfiguration.JedisPoolingClientConfigurationBuilder) JedisClientConfiguration.builder();
        JedisClientConfiguration clientConfiguration = jpcb.build();
        return new JedisConnectionFactory(config, clientConfiguration);
    }

    @Bean(name = "phoneCodeRedisTemplate")
    public StringRedisTemplate phoneRedisTemplate() {
        StringRedisTemplate redisTemplate = new StringRedisTemplate();
        redisTemplate.setConnectionFactory(redisConnectionFactory(telephoneCodeHost,
                telephoneCodePort,
                telephoneCodeDBPass,
                maxIdle,
                maxTotal,
                maxWaitMillis,
                telephoneCodeDB));
        return redisTemplate;
    }

    @Bean(name = "emailCodeRedisTemplate")
    public StringRedisTemplate emailRedisTemplate() {
        StringRedisTemplate redisTemplate = new StringRedisTemplate();
        redisTemplate.setConnectionFactory(redisConnectionFactory(emailCodeHost,
                emailCodePort,
                emailCodeDBPass,
                maxIdle,
                maxTotal,
                maxWaitMillis,
                emailCodeDB));
        return redisTemplate;
    }

    @Bean(name = "updatePhoneRedisTemplate")
    public StringRedisTemplate updatePhoneRedisTemplate() {
        StringRedisTemplate redisTemplate = new StringRedisTemplate();
        redisTemplate.setConnectionFactory(redisConnectionFactory(updatePhoneCodeHost,
                updatePhoneCodePort,
                updatePhoneCodePass,
                maxIdle,
                maxTotal,
                maxWaitMillis,
                updatePhoneCodeDB));
        return redisTemplate;
    }

    @Bean(name = "updateEmailRedisTemplate")
    public StringRedisTemplate updateEmailRedisTemplate() {
        StringRedisTemplate redisTemplate = new StringRedisTemplate();
        redisTemplate.setConnectionFactory(redisConnectionFactory(updateEmailCodeHost,
                updateEmailCodePort,
                updateEmailCodePass,
                maxIdle,
                maxTotal,
                maxWaitMillis,
                updateEmailCodeDB));
        return redisTemplate;
    }

}
