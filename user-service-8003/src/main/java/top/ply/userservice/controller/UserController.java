package top.ply.userservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import top.ply.common_unit.entity.RespEntity;
import top.ply.userservice.service.impl.CommonUserServiceFacade;

@RestController
public class UserController {

    @Autowired
    CommonUserServiceFacade service;

    @PostMapping("/bind/password")
    public RespEntity bindPassword(String token, String password) {
        return service.bindPassword(token, password);
    }

    @PostMapping("/bind/username")
    public RespEntity bindUsername(String token, String username) {
        return service.bindUsername(token, username);
    }

    @PostMapping("/bind/get_code")
    public RespEntity getBindCode(String token, String address) {
        return service.sendBindCode(token, address);
    }

    @PostMapping("/bind/action")
    public RespEntity doBindWithCode(String token ,String address, String code) {
        return service.doBindWithCode(token, address, code);
    }

    @PostMapping("/update/password")
    public RespEntity updatePassword(String token, String oldPassword, String password) {
        return service.updatePassword(token, oldPassword, password);
    }

    @PostMapping("/update/username")
    public RespEntity updateUsername(String token, String username) {
        return service.updateUsername(token, username);
    }

    @PostMapping("/update/get_code")
    public RespEntity getUpdateCode(String token, String route) {
        return service.sendUpdateCode(token, route);
    }

    @PostMapping("/update/action")
    public RespEntity doUpdateWithCode(String token, String code, String newAddress) {
        return service.doUpdateWithCode(token, code, newAddress);
    }

}
