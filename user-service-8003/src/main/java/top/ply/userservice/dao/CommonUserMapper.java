package top.ply.userservice.dao;

import org.springframework.stereotype.Repository;
import top.ply.authservice.pojo.CommonUser;

@Repository
public interface CommonUserMapper {

    public CommonUser getCommonUserByID(String userID);

    public int updateCommonUser(CommonUser commonUser);

    public CommonUser getCommonUserByEmail(String email);

    public CommonUser getCommonUserByUsername(String username);

    public CommonUser getCommonUserByTelephone(String telephone);

}
