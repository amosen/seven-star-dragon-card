package top.ply.userservice.service;

import top.ply.common_unit.entity.RespEntity;

public interface CommonUserService {

    RespEntity bindPassword(String token, String password);

    RespEntity bindUsername(String token,  String username);

    RespEntity sendBindTelephoneVerifyCode(String token, String telephone);

    RespEntity doBindTelephoneWithVerifyCode(String token, String telephone, String code);

    RespEntity sendBindEmailVerifyCode(String token, String email);

    RespEntity doBindEmailWithVerifyCode(String token, String email, String code);

    RespEntity updatePassword(String token, String oldPassword, String newPassword);

    RespEntity updateUsername(String token, String username);

    RespEntity sendUpdateTelephoneVerifyCode(String token);

    RespEntity doUpdateTelephoneWithVerifyCode(String token, String code, String newPhone);

    RespEntity sendUpdateEmailVerifyCode(String token);

    RespEntity doUpdateEmailWithVerifyCode(String token, String code, String newMail);

}
