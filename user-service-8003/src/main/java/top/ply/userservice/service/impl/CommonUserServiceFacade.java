package top.ply.userservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.ply.authservice.service.CommonAuthService;
import top.ply.common_unit.entity.RespEntity;
import top.ply.common_unit.global_resp.RespUtil;
import top.ply.message.service.EmailMsgService;

@Service
public class CommonUserServiceFacade {

    @Autowired
    CommonUserServiceImpl service;

    public static final String EMAIL_ROUTE = "email";

    public static final String PHONE_ROUTE = "telephone";

    public RespEntity bindPassword(String token, String password) {
        return service.bindPassword(token, password);
    }

    public RespEntity bindUsername(String token, String username) {
        return service.bindUsername(token, username);
    }

    public RespEntity sendBindCode(String token, String address) {
        if (CommonAuthService.EMAIL_PATTERN.matcher(address).matches()) {
            return service.sendBindEmailVerifyCode(token, address);
        } else if (CommonAuthService.PHONE_PATTERN.matcher(address).matches()) {
            return service.sendBindTelephoneVerifyCode(token, address);
        } else {
            return RespUtil.illegalParameter();
        }
    }

    public RespEntity doBindWithCode(String token, String address, String code) {
        if (CommonAuthService.PHONE_PATTERN.matcher(address).matches()) {
            return service.doBindTelephoneWithVerifyCode(token, address, code);
        } else if (CommonAuthService.EMAIL_PATTERN.matcher(address).matches()) {
            return service.doBindEmailWithVerifyCode(token, address, code);
        } else {
            return RespUtil.illegalParameter();
        }
    }

    public RespEntity updatePassword(String token, String oldPassword, String newPassword) {
        return service.updatePassword(token, oldPassword, newPassword);
    }

    public RespEntity updateUsername(String token, String username) {
        return service.updateUsername(token, username);
    }

    public RespEntity sendUpdateCode(String token, String route) {
        if (route.equalsIgnoreCase(EMAIL_ROUTE)) {
            return service.sendUpdateEmailVerifyCode(token);
        } else if (route.equalsIgnoreCase(PHONE_ROUTE)) {
            return service.sendUpdateTelephoneVerifyCode(token);
        } else {
            return RespUtil.illegalParameter();
        }
    }

    public RespEntity doUpdateWithCode(String token, String code, String newAddress) {
        if (CommonAuthService.EMAIL_PATTERN.matcher(newAddress).matches()) {
            return service.doUpdateEmailWithVerifyCode(token, code, newAddress);
        } else if (CommonAuthService.PHONE_PATTERN.matcher(newAddress).matches()) {
            return service.doUpdateTelephoneWithVerifyCode(token, code, newAddress);
        } else {
            return RespUtil.illegalParameter();
        }
    }
}
