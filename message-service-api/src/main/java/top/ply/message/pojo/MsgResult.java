package top.ply.message.pojo;

public class MsgResult {

    private String msgID;

    private String bizId;

    private Integer statusCode;

    public MsgResult() {
    }


    public MsgResult(String msgID, String bizId, Integer statusCode) {
        this.msgID = msgID;
        this.bizId = bizId;
        this.statusCode = statusCode;
    }

    public String getMsgID() {
        return msgID;
    }

    public void setMsgID(String msgID) {
        this.msgID = msgID;
    }

    public String getBizId() {
        return bizId;
    }

    public void setBizId(String bizId) {
        this.bizId = bizId;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "MsgResult{" +
                "msgID='" + msgID + '\'' +
                ", bizId='" + bizId + '\'' +
                ", statusCode='" + statusCode + '\'' +
                '}';
    }
}
