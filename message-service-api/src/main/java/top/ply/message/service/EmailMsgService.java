package top.ply.message.service;

import top.ply.message.pojo.MsgResult;

/**
 * @Author: Amosen
 *
 * 邮箱消息服务接口
 */
public interface EmailMsgService {

    static final String sendToKey = "email";

    static final String verifyCodeKey = "code";

    /**
     * 验证码下发接口
     * @param email
     * @param code
     */
    boolean sendVerifyCode(String email, String code);

    /**
     * 获取消息下发结果查询接口
     * @param msgID
     * @return
     */
    MsgResult getMsgResult(String msgID);

}
