package top.ply.message.service;

import top.ply.message.pojo.MsgResult;

public interface TelephoneMsgService {

    static final String sendToKey = "telephone";

    static final String VERIFY_CODE_KEY = "code";

    boolean sendVerifyCode(String phoneNumbers, String code);

    MsgResult getMsgResult(String msgID);

}
