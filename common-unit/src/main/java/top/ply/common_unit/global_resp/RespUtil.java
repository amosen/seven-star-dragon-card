package top.ply.common_unit.global_resp;

import top.ply.common_unit.entity.RespEntity;

import java.util.Map;

public class RespUtil {

    public static final Integer GLOBAL_SUCCESS_CODE = 200;

    public static final String GLOBAL_SUCCESS_MSG = "request: success";

    public static final Integer GLOBAL_FAIL_CODE = 500;

    public static final String GLOBAL_FAIL_MSG = "request: fail";

    public static final Integer ILLEGAL_PARAMETER_CODE = 10001;

    public static final String ILLEGAL_PARAMETER_MSG = "Illegal parameters";

    public static final Integer TOKEN_VERIFY_FAIL_ERROR_CODE = 10002;

    public static final String TOKEN_VERIFY_FAIL_ERROR_MSG = "Token verify failed";

    public static final Integer VERIFY_CODE_ERROR_CODE = 10003;

    public static final String VERIFY_CODE_ERROR_MSG = "Verify code error";

    public static final Integer USER_STATUS_ERROR_CODE = 20001;

    public static final String USER_STATUS_ERROR_MSG = "User status error";

    public static final Integer CONFLICT_ERROR_CODE = 20002;

    public static final String CONFLICT_ERROR_MSG = "Parameter try to operate is conflicted";

    public static final Integer USER_PASSWORD_ERROR_CODE = 30001;

    public static final String USER_PASSWORD_ERROR_MSG = "Your password or account is wrong";

    public static final Integer ACCOUNT_LOCKED_ERROR_CODE = 30002;

    public static final String ACCOUNT_LOCKED_ERROR_MSG = "The account try to login has been locked";

    public static final Integer BAD_CREDENTIALS_ERROR_CODE = 30003;

    public static final String BAD_CREDENTIALS_ERROR_MSG = "Wrong password";

    public static final Integer ACCOUNT_DISABLED_ERROR_CODE = 30004;

    public static final String ACCOUNT_DISABLED_ERROR_MSG = "The account try to login has been disabled";

    public static final Integer ACCOUNT_EXPIRED_ERROR_CODE = 30005;

    public static final String ACCOUNT_EXPIRED_ERROR_MSG = "The account try to login has been expired";

    public static final Integer CREDENTIALS_EXPIRED_ERROR_CODE = 30006;

    public static final String CREDENTIALS_EXPIRED_ERROR_MSG = "The password used has been expired";


    public static RespEntity baseSuccess() {
        return new RespEntity(GLOBAL_SUCCESS_CODE, GLOBAL_SUCCESS_MSG);
    }

    public static RespEntity baseSuccess(Map<String, Object> data) {
        return new RespEntity(GLOBAL_SUCCESS_CODE, GLOBAL_SUCCESS_MSG, data);
    }

    public static RespEntity baseFail() {
        return new RespEntity(GLOBAL_FAIL_CODE, GLOBAL_FAIL_MSG);
    }

    public static RespEntity baseFail(Map<String, Object> data) {
        return new RespEntity(GLOBAL_FAIL_CODE, GLOBAL_FAIL_MSG, data);
    }

    public static RespEntity illegalParameter() {
        return new RespEntity(ILLEGAL_PARAMETER_CODE, ILLEGAL_PARAMETER_MSG);
    }

    public static RespEntity userStatusError() {
        return new RespEntity(USER_STATUS_ERROR_CODE, USER_STATUS_ERROR_MSG);
    }

    public static RespEntity userPasswordError() {
        return new RespEntity(USER_PASSWORD_ERROR_CODE, USER_PASSWORD_ERROR_MSG);
    }

    public static RespEntity tokenVerifyError() {
        return new RespEntity(TOKEN_VERIFY_FAIL_ERROR_CODE, TOKEN_VERIFY_FAIL_ERROR_MSG);
    }

    public static RespEntity verifyCodeError() {
        return new RespEntity(VERIFY_CODE_ERROR_CODE, VERIFY_CODE_ERROR_MSG);
    }

    public static RespEntity conflictError() {
        return new RespEntity(CONFLICT_ERROR_CODE, CONFLICT_ERROR_MSG);
    }

    public static RespEntity accountLockedError() {
        return new RespEntity(ACCOUNT_LOCKED_ERROR_CODE, ACCOUNT_LOCKED_ERROR_MSG);
    }

    public static RespEntity badCredentialsError() {
        return new RespEntity(BAD_CREDENTIALS_ERROR_CODE, BAD_CREDENTIALS_ERROR_MSG);
    }

    public static RespEntity accountDisabledError() {
        return new RespEntity(ACCOUNT_DISABLED_ERROR_CODE, ACCOUNT_DISABLED_ERROR_MSG);
    }

    public static RespEntity accountExpiredError() {
        return new RespEntity(ACCOUNT_EXPIRED_ERROR_CODE, ACCOUNT_EXPIRED_ERROR_MSG);
    }

    public static RespEntity credentialsExpiredError() {
        return new RespEntity(CREDENTIALS_EXPIRED_ERROR_CODE, CREDENTIALS_EXPIRED_ERROR_MSG);
    }
}
