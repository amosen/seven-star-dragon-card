package top.ply.common_unit.constant;

import java.util.regex.Pattern;

public class ConstantPool {

    public static final String EMAIL_REG_EXP = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";

    public static final String PHONE_REG_EXP = "^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$";

    public static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REG_EXP);

    public static final Pattern PHONE_PATTERN = Pattern.compile(PHONE_REG_EXP);
    
}
