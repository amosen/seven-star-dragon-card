package top.ply.common_unit.entity;

import java.util.Map;

public class RespEntity {

    private Integer code;

    private String msg;

    private Map<String, Object> data;

    public RespEntity() {
    }

    public RespEntity(Integer code, String msg, Map data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public RespEntity(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map data) {
        this.data = data;
    }
}
