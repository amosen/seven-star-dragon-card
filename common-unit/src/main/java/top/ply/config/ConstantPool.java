package top.ply.config;

public class ConstantPool {

    public static final Integer NET_SOCKET_TIMEOUT = 30 * 1000;

    public static final Integer NET_CONNECTION_TIMEOUT = 30 * 1000;

    public static final Integer NET_MAX_CONNECTION = 100;

    public static final Integer NET_MAX_CONCURRENT_CONNECTION = 100;

    public static final Integer NET_MAX_FAIL_TIME = 3;

}
