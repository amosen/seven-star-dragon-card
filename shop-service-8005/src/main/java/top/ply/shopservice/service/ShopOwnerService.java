package top.ply.shopservice.service;

import top.ply.common_unit.entity.RespEntity;

public interface ShopOwnerService {

    RespEntity sendRegisterCode(String telephone);

    void checkRegisterCode(String telephone, String code);

}
