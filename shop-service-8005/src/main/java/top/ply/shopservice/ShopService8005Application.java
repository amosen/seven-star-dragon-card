package top.ply.shopservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopService8005Application {

    public static void main(String[] args) {
        SpringApplication.run(ShopService8005Application.class, args);
    }

}
