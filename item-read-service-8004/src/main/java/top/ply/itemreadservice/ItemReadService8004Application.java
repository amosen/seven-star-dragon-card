package top.ply.itemreadservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItemReadService8004Application {

    public static void main(String[] args) {
        SpringApplication.run(ItemReadService8004Application.class, args);
    }

}
