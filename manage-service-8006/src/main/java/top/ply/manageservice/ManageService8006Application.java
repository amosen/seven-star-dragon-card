package top.ply.manageservice;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@MapperScan(basePackages = {"top.ply.manageservice.dao"})
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ManageService8006Application {

    public static void main(String[] args) {
        SpringApplication.run(ManageService8006Application.class, args);
    }

}
