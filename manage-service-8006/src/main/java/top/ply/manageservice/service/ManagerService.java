package top.ply.manageservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import top.ply.manageservice.dao.ManagerMapper;
import top.ply.manageservice.pojo.Manager;

public interface ManagerService extends UserDetailsService {

    boolean verifyToken(String token);

    String generateToken(String managerID);

    Manager getManagerFromToken(String token);

}
