package top.ply.manageservice.dao;

import org.apache.ibatis.annotations.Mapper;
import top.ply.manageservice.pojo.Manager;

@Mapper
public interface ManagerMapper {

    Manager getManagerByUsername(String username);

    Manager getManagerByID(String managerID);

    int addManager(Manager manager);

}
