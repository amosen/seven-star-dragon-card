package top.ply.manageservice.dao;

import org.apache.ibatis.annotations.Mapper;
import top.ply.manageservice.pojo.Role;

import java.util.List;
import java.util.Map;

@Mapper
public interface RoleMapper {

     Map<Integer, String> getRoles();

}
