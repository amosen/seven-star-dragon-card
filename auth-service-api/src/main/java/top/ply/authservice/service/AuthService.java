package top.ply.authservice.service;

import java.util.Map;

/**
 * @Author: Amosen
 * @School: Neuq
 *
 * 对普通用户的登录、校验服务
 */
public interface AuthService {

    static final String PAYLOAD_USER_ID_KEY = "userID";

    static final String TOKEN_KEY = "token";



    /**
     * 校验token是否有效，过期、不合法的token都将校验不通过并返回false
     * @param token 待校验的token
     * @return 校验是否通过
     */
    boolean verifyToken(String token);

    /**
     * 从token中获取到用户ID，这个ID是全局唯一的普通用户ID，其他数据可通过这个ID绑定唯一用户
     * 如果token校验不通过，则这个接口会返回null
     * @param token 被下发的token
     * @return 用户ID
     */
    String getUserIDFromToken(String token);

    /**
     * 刷新token过期时间，这里将会进行一次token的校验，如果token校验不通过，则返回null
     * @param token 旧的token
     * @param expireTime 新token的过期时间
     * @return 新的token
     */
    String refreshToken(String token);

}
