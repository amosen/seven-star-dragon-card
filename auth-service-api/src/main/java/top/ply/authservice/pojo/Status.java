package top.ply.authservice.pojo;

public class Status {

    private int statusID;

    private Integer statusCode;

    private String status;

    public Status() {
    }

    public Status(int statusID, Integer statusCode, String status) {
        this.statusID = statusID;
        this.statusCode = statusCode;
        this.status = status;
    }

    public int getStatusID() {
        return statusID;
    }

    public void setStatusID(int statusID) {
        this.statusID = statusID;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Status{" +
                "statusID=" + statusID +
                ", statusCode=" + statusCode +
                ", status='" + status + '\'' +
                '}';
    }
}
