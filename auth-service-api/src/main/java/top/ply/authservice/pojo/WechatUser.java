package top.ply.authservice.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class WechatUser extends BaseUser {

    @JsonIgnore
    private String openid;

    public WechatUser(long id, String userID, Status status, String openid) {
        super(id, userID, status);
        this.openid = openid;
    }

    public WechatUser() {
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    @Override
    public String toString() {
        return "WechatUser{" +
                "openid='" + openid + '\'' +
                '}';
    }
}
