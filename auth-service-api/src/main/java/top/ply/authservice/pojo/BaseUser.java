package top.ply.authservice.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BaseUser {

    @JsonIgnore
    private long id;

    @JsonIgnore
    private String userID;

    @JsonIgnore
    private Status status;

    public BaseUser(long id, String userID, Status status) {
        this.id = id;
        this.userID = userID;
        this.status = status;
    }

    public BaseUser() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BaseUser{" +
                "id=" + id +
                ", userID=" + userID +
                ", status=" + status +
                '}';
    }
}
