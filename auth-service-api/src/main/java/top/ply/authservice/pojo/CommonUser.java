package top.ply.authservice.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CommonUser extends BaseUser {

    private String telephone;

    @JsonIgnore
    private String password;

    private String username;

    private String email;

    public CommonUser(long id, String userID, Status status, String telephone, String password, String username, String email) {
        super(id, userID, status);
        this.telephone = telephone;
        this.password = password;
        this.username = username;
        this.email = email;
    }

    public CommonUser() {

    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "CommonUser{" +
                "telephone='" + telephone + '\'' +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
