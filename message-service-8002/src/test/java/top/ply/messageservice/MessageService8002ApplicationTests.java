package top.ply.messageservice;


import com.rabbitmq.client.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootTest
public class MessageService8002ApplicationTests {

    @Value("${spring.rabbitmq.verify-code-exchange.name}")
    public String verifyCodeExchange;

    @Value("${spring.rabbitmq.email-verify-code-queue.name}")
    public String emailVerifyCodeQueueName;

    @Value("${spring.rabbitmq.email-verify-code-queue.routingKey}")
    public String emailVerifyCodeRoutingKey;

    @Value("${spring.rabbitmq.host}")
    public String rabbitMQHost;

    @Value("${spring.rabbitmq.port}")
    public Integer rabbitMQPort;

    @Value("${spring.rabbitmq.username}")
    public String rabbitMQUser;

    @Value("${spring.rabbitmq.password}")
    public String  rabbitMQPassword;

    @Test
    public void testConsumer() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.152.128");
        factory.setPort(5672);
        factory.setUsername("root");
        factory.setPassword("root");
        Connection connection = factory.newConnection();
        DeliverCallback callback = new DeliverCallback() {
            @Override
            public void handle(String consumerTag, Delivery message) throws IOException {
                String threadName = Thread.currentThread().getName();
                byte[] body = message.getBody();
                String s = new String(body);
                System.out.println("Thread: " + threadName + " received: " + s);
            }
        };
        for (int i = 0; i < 2; i++) {
            Thread t = new Thread(() -> {
                try {
                    Channel channel = connection.createChannel();
                    channel.queueBind("email-verify-code", "verify-code-exchange", "emailVerifyCode");
                    channel.basicConsume("email-verify-code", true, callback, consumerTag -> {
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            t.setName("Thread-" + ++i);
            t.start();
        }
    }

}
