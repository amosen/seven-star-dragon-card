package top.ply.messageservice.config;

public class ConstantPool {

    public static final Integer MSG_SEND_SUCCESS_CODE = 200;

    public static final Integer MSG_SEND_FAIL_CODE = 500;

}
