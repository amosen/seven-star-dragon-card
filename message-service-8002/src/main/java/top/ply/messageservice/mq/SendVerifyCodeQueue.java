package top.ply.messageservice.mq;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface SendVerifyCodeQueue {

    static final String EMAIL_CODE_QUEUE = "email-verify-code-queue";

    static final String PHONE_CODE_QUEUE = "phone-verify-code-queue";

    @Input(EMAIL_CODE_QUEUE)
    SubscribableChannel emailCodeChannel();

    @Input(PHONE_CODE_QUEUE)
    SubscribableChannel phoneCodeChannel();

}
