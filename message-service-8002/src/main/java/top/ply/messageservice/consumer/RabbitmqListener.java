package top.ply.messageservice.consumer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.http.client.domain.ExchangeType;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import top.ply.message.service.EmailMsgService;
import top.ply.message.service.TelephoneMsgService;
import top.ply.messageservice.service.Impl.EmailMsgServiceImpl;
import top.ply.messageservice.service.Impl.TelephoneMsgServiceImpl;

import java.io.IOException;
import java.util.Map;

@Component
public class RabbitmqListener {

    @Autowired
    EmailMsgServiceImpl emailMsgService;

    @Autowired
    TelephoneMsgServiceImpl telephoneMsgService;

    @RabbitHandler
    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue(name = "${spring.rabbitmq.email-verify-code-queue.name}", durable = "false", autoDelete = "true", exclusive = "false"),
                          exchange = @Exchange(name = "${spring.rabbitmq.email-verify-code-queue.exchange}",
                                  durable = Exchange.FALSE,
                                  type = ExchangeTypes.TOPIC,
                                  autoDelete = Exchange.TRUE),
                          key = "${spring.rabbitmq.email-verify-code-queue.routingKey}"
                          )
    }, errorHandler = "emailMsgMQErrorHandler")
    public void emailVerifyCodeListener(@Payload Map<String, String> mailAndCode, Message message, Channel channel) throws IOException {

        if (mailAndCode.containsKey(EmailMsgService.sendToKey) && mailAndCode.containsKey(EmailMsgService.verifyCodeKey)) {
            String sendTo = mailAndCode.get(EmailMsgService.sendToKey);
            String code = mailAndCode.get(EmailMsgService.verifyCodeKey);
            if (StringUtils.hasText(sendTo) && StringUtils.hasText(code)) {
                emailMsgService.sendVerifyCode(sendTo, code);
                channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            }
        }

    }

    @RabbitHandler
    @RabbitListener(bindings = {
            @QueueBinding(value = @Queue(name = "${spring.rabbitmq.phone-verify-code-queue.name}", durable = "false", autoDelete = "true", exclusive = "false"),
                    exchange = @Exchange(name = "${spring.rabbitmq.phone-verify-code-queue.exchange}",
                            durable = Exchange.FALSE,
                            type = ExchangeTypes.TOPIC,
                            autoDelete = Exchange.TRUE),
                    key = "${spring.rabbitmq.phone-verify-code-queue.routingKey}"
            )
    }, errorHandler = "phoneMsgMQErrorHandler")
    public void telephoneVerifyCodeListener(@Payload Map<String, String> phoneAndCode, Message message, Channel channel) throws IOException {

        if (phoneAndCode.containsKey(TelephoneMsgService.sendToKey) && phoneAndCode.containsKey(TelephoneMsgService.VERIFY_CODE_KEY)) {
            String sendTo = phoneAndCode.get(TelephoneMsgService.sendToKey);
            String code = phoneAndCode.get(TelephoneMsgService.VERIFY_CODE_KEY);
            if (StringUtils.hasText(sendTo) && StringUtils.hasText(code)) {
                telephoneMsgService.sendVerifyCode(sendTo, code);
                channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            }
        }

    }

}
