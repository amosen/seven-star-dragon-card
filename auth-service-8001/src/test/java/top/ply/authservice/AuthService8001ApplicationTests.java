package top.ply.authservice;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import top.ply.authservice.service.impl.WechatAuthServiceImpl;
import top.ply.common_unit.token.JWTUtil;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthService8001ApplicationTests {

    @Autowired
    @Qualifier("wechatAuthService")
    WechatAuthServiceImpl wechatAuthService;

    @Test
    public void testPayload() {
        Map<String, String> payload = new HashMap<>();
        payload.put("username", "王小美");
        payload.put("password", "7777777777777");
        String token = JWTUtil.createToken(payload, "1234", 3 * 60 * 1000);
        Map payload1 = JWTUtil.getPayload(token, "1234");
        String token1 = JWTUtil.createToken(payload1, "1234", 30 * 60 * 1000);
        Map payload2 = JWTUtil.getPayload(token1, "1234");
        System.out.println("payload2" + payload2);

    }

    @Test
    public void testWechatAuthServiceLogin() {
        System.out.println("开始微信认证服务测试");
        String result = wechatAuthService.wechatUserLogin("11111");
        System.out.println(result);
    }

}
