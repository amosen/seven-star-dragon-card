package top.ply.authservice.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Value("${spring.rabbitmq.email-verify-code-queue.name}")
    private String emailVerifyCodeQueueName;

    @Value("${spring.rabbitmq.email-verify-code-queue.routingKey}")
    private String emailVerifyCodeQueueRoutingKey;

    @Value("${spring.rabbitmq.verify-code-exchange.name}")
    private String verifyCodeExchangeName;

    @Value("${spring.rabbitmq.phone-verify-code-queue.name}")
    private String phoneVerifyCodeQueueName;

    @Value("${spring.rabbitmq.phone-verify-code-queue.routingKey}")
    private String phoneVerifyCodeQueueRoutingKey;

    @Value("${spring.rabbitmq.user-login-event-publish-exchange.name}")
    private String userLoginEventExchange;

    @Value("${spring.rabbitmq.user-login-event-publish-queue.routingKey}")
    private String userLoginEventQueueRoutingKey;

    @Bean("${spring.rabbitmq.email-verify-code-queue.name}")
    public Queue emailVerifyCodeQueue() {
        return new Queue(emailVerifyCodeQueueName, false, false, true);
    }

    @Bean("${spring.rabbitmq.verify-code-exchange.name}")
    public TopicExchange verifyCodeTopicExchange() {
        return new TopicExchange(verifyCodeExchangeName, false, true);
    }

    @Bean
    public Binding emailVerifyCodeQueueBinding(@Qualifier("${spring.rabbitmq.email-verify-code-queue.name}") Queue emailVerifyCodeQueue,
                                               @Qualifier("${spring.rabbitmq.verify-code-exchange.name}") Exchange verifyCodeExchange) {

        return BindingBuilder.bind(emailVerifyCodeQueue)
                .to(verifyCodeExchange)
                .with(emailVerifyCodeQueueRoutingKey)
                .noargs();

    }

    @Bean("${spring.rabbitmq.phone-verify-code-queue.name}")
    public Queue phoneVerifyCodeQueue() {
        return new Queue(phoneVerifyCodeQueueName, false, false, true);
    }

    @Bean
    public Binding phoneVerifyCodeQueueBinding(@Qualifier("${spring.rabbitmq.phone-verify-code-queue.name}") Queue phoneVerifyCodeQueue,
                                               @Qualifier("${spring.rabbitmq.verify-code-exchange.name}") Exchange verifyCodeExchange) {

        return BindingBuilder.bind(phoneVerifyCodeQueue)
                .to(verifyCodeExchange)
                .with(phoneVerifyCodeQueueRoutingKey)
                .noargs();

    }

    @Bean("${spring.rabbitmq.user-login-event-publish-exchange.name}")
    public Exchange userLoginEventPublishExchange() {
        return new FanoutExchange(userLoginEventExchange);
    }

    @Bean
    public Binding userLoginEventPublishBinding(@Qualifier("${spring.rabbitmq.user-login-event-publish-exchange.name}") Exchange publishExchange) {
        return BindingBuilder.bind(publishExchange)
                .to(publishExchange)
                .with(userLoginEventQueueRoutingKey)
                .noargs();
    }

}
