package top.ply.authservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class CustomRedisTemplate {

    @Value("${spring.redis.telephoneCode.host}")
    private String telephoneCodeHost;

    @Value("${spring.redis.telephoneCode.port}")
    private int telephoneCodePort;

    @Value("${spring.redis.telephoneCode.db}")
    private int telephoneCodeDB;

    @Value("${spring.redis.telephoneCode.password}")
    private String telephoneCodeDBPass;

    @Value("${spring.redis.emailCode.host}")
    private String emailCodeHost;

    @Value("${spring.redis.emailCode.port}")
    private int emailCodePort;

    @Value("${spring.redis.emailCode.db}")
    private int emailCodeDB;

    @Value("${spring.redis.emailCode.password}")
    private String emailCodeDBPass;

    @Value("${spring.redis.max-idle}")
    private int maxIdle;

    @Value("${spring.redis.max-total}")
    private int maxTotal;

    @Value("${spring.redis.max-wait-millis}")
    private long maxWaitMillis;

    @Value("${spring.redis.testOnBorrow}")
    private boolean testOnBorrow;

    public JedisPoolConfig jedisPoolConfig(int maxIdle, int maxTotal, long maxWaitMillis, boolean testOnBorrow) {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxIdle(maxIdle);
        config.setMaxTotal(maxTotal);
        config.setMaxWaitMillis(maxWaitMillis);
        config.setTestOnBorrow(testOnBorrow);
        return config;
    }

    public RedisConnectionFactory redisConnectionFactory(String host, int port, String password, int maxIdle,
                                                         int maxTotal, long maxWaitMillis, int index) {
        RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
        config.setHostName(host);
        config.setPort(port);
        config.setDatabase(index);
        config.setPassword(RedisPassword.of(password));
        JedisClientConfiguration.JedisPoolingClientConfigurationBuilder jpcb =
                (JedisClientConfiguration.JedisPoolingClientConfigurationBuilder) JedisClientConfiguration.builder();
        JedisClientConfiguration clientConfig = jpcb.build();
        return new JedisConnectionFactory(config, clientConfig);
    }

    @Bean(name = "telephoneCodeRedisTemplate")
    public StringRedisTemplate telephoneCodeTemplate() {
        StringRedisTemplate redisTemplate = new StringRedisTemplate();
        redisTemplate.setConnectionFactory(
                redisConnectionFactory(telephoneCodeHost,
                        telephoneCodePort,
                        telephoneCodeDBPass,
                        maxIdle,
                        maxTotal,
                        maxWaitMillis,
                        telephoneCodeDB)
        );
        return redisTemplate;
    }

    @Bean(name = "emailCodeRedisTemplate")
    public StringRedisTemplate emailCodeTemplate() {
        StringRedisTemplate redisTemplate = new StringRedisTemplate();
        redisTemplate.setConnectionFactory(
                redisConnectionFactory(emailCodeHost,
                        emailCodePort,
                        emailCodeDBPass,
                        maxIdle,
                        maxTotal,
                        maxWaitMillis,
                        emailCodeDB)
        );
        return redisTemplate;
    }
}
