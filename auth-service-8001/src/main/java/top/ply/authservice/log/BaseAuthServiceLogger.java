package top.ply.authservice.log;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class BaseAuthServiceLogger {

    Logger logger = LoggerFactory.getLogger(BaseAuthServiceLogger.class);

    @Pointcut("execution(* top.ply.authservice.service.impl.BaseAuthService.*(..))")
    void pointcut(){}

    @Around("pointcut()")
    public Object baseAuthServiceActionsLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        if (logger.isInfoEnabled()) {
            logger.info("Enter method: "
                    + joinPoint.getSignature().getName()
                    + " (" + joinPoint.getTarget().getClass().getName() +")");
            logger.info("args: {}", joinPoint.getArgs());
        }
        try {
            Object result = joinPoint.proceed();
            if (logger.isInfoEnabled()) {
                logger.info("Escape method (" + joinPoint.getSignature().getName() + ") with result: " + result);
            }
            return result;
        } catch (Throwable throwable) {
            logger.error("Error when proceed method: "
                    + joinPoint.getSignature().getName() + " {}", throwable.getStackTrace());
            throw throwable;
        }
    }

}
