package top.ply.authservice.log;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import top.ply.authservice.service.impl.CommonLoginWorkerImpl;

@Aspect
@Component
public class CommonLoginWorkerLogger {
    
    Logger logger = LoggerFactory.getLogger(CommonLoginWorkerImpl.class);
    
    @Around("execution(* top.ply.authservice.service.impl.CommonLoginWorkerImpl.getUserIDFromToken(..))")
    public Object getUserIDFromTokenLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        logger.info("Getting userID from token: " + args[0]);
        try {
            Object result = joinPoint.proceed();
            logger.info("Got userID: " + result);
            return result;
        } catch (Throwable throwable) {
            logger.error("Error when getting userID from token: " + args[0] + "{}", throwable.getStackTrace());
            throw throwable;
        }
    }

    @Around("execution(* top.ply.authservice.service.impl.CommonLoginWorkerImpl.isNormal(..))")
    public Object isNormalLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        if (logger.isInfoEnabled()) {
            logger.info("Deciding is normal or not with token: " + args[0]);
        }
        try {
            Object result = joinPoint.proceed();
            logger.info("User state is normal: " + result);
            return result;
        } catch (Throwable throwable) {
            logger.error("Error when judge user state is normal or not with token" + args[0] + "{}", throwable.getStackTrace());
            throw throwable;
        }
    }

    @Around("execution(* top.ply.authservice.service.impl.CommonLoginWorkerImpl.commonLoginWithEmail(..))")
    public Object emailLoginLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        if (logger.isInfoEnabled()) {
            logger.info("Accepted login action (password) with email: " + args[0]);
        }
        try {
            Object result = joinPoint.proceed();
            logger.info("Generated token for email: " + args[0]);
            logger.info("reason: " + ((CommonLoginWorkerImpl) joinPoint.getThis()).getErrReason());
            return result;
        } catch (Throwable throwable) {
            logger.error("Error when proceeding loginAction with email: " + args[0] + "{}", throwable.getStackTrace());
            throw throwable;
        }
    }

    @Around("execution(* top.ply.authservice.service.impl.CommonLoginWorkerImpl.commonLoginWithTelephone(..))")
    public Object telephoneLoginLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        if (logger.isInfoEnabled()) {
            logger.info("Accepted login action (password) with telephone: " + args[0]);
        }
        try {
            Object result = joinPoint.proceed();
            logger.info("Generated token for telephone: " + args[0]);
            logger.info("reason: " + ((CommonLoginWorkerImpl) joinPoint.getThis()).getErrReason());
            return result;
        } catch (Throwable throwable) {
            logger.error("Error when proceeding loginAction with telephone: " + args[0] + "{}", throwable.getStackTrace());
            throw throwable;
        }
    }

    @Around("execution(* top.ply.authservice.service.impl.CommonLoginWorkerImpl.commonLoginWithAccount(..))")
    public Object accountLoginLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        if (logger.isInfoEnabled()) {
            logger.info("Accepted login action (password) with username: " + args[0]);
        }
        try {
            Object result = joinPoint.proceed();
            logger.info("Generated token for username: " + args[0]);
            logger.info("reason: " + ((CommonLoginWorkerImpl) joinPoint.getThis()).getErrReason());
            return result;
        } catch (Throwable throwable) {
            logger.error("Error when proceeding loginAction with username: " + args[0] + "{}", throwable.getStackTrace());
            throw throwable;
        }
    }

    @Around("execution(* top.ply.authservice.service.impl.CommonLoginWorkerImpl.sendCodeWithEmail(..))")
    public Object sendCodeWithEmailLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        if (logger.isInfoEnabled()) {
            logger.info("Sending verify code to email: " + args[0]);
        }
        try {
            Object result = joinPoint.proceed();
            if (logger.isInfoEnabled()) {
                logger.info("Sending verify code result: " + result);
            }
            return result;
        } catch (Throwable throwable) {
            logger.error("Error when sending code to email: " + args[0] + "{}", throwable.getStackTrace());
            throw throwable;
        }
    }

    @Around("execution(* top.ply.authservice.service.impl.CommonLoginWorkerImpl.sendCodeWithEmail(..))")
    public Object checkUserStatusLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        if (logger.isInfoEnabled()) {
            logger.info("Checking user status, user: " + args[0]);
        }
        try {
            Object result = joinPoint.proceed();
            if (logger.isInfoEnabled()) {
                logger.info("User status: " + result);
            }
            return result;
        } catch (Throwable throwable) {
            logger.error("Error when checking user status: {}", throwable.getStackTrace());
            throw throwable;
        }
    }


    
}   
