package top.ply.authservice.log;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class WechatLoginWorkerLogger {

    Logger logger = LoggerFactory.getLogger(WechatLoginWorkerLogger.class);

    @Around("execution(* top.ply.authservice.service.impl.WechatLoginWorkerImpl.wechatUserLogin(..))")
    public Object loginActionLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        if (logger.isInfoEnabled()) {
            logger.info("Accept js_code: " + args[0]);
        }
        try {
            Object result = joinPoint.proceed();
            if (logger.isInfoEnabled()) {
                logger.info("Generated token with: " + result + "for js_code: " + args[0]);
            }
            return result;
        } catch (Throwable throwable) {
            logger.error("Error when proceed wechatLogin action: {}", throwable.getStackTrace());
            throw throwable;
        }
    }

    @Around("execution(* top.ply.authservice.service.impl.WechatLoginWorkerImpl.checkParam(..))")
    public Object checkParamActionLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        if (logger.isInfoEnabled()) {
            logger.info("Checking parameters: " + Arrays.toString(args));
        }
        try {
            Object result = joinPoint.proceed();
            if (logger.isInfoEnabled()) {
                logger.info("Check parameter result: " + result);
            }
            return result;
        } catch (Throwable throwable) {
            logger.error("Error when checking parameters: {}", throwable.getStackTrace());
            throw throwable;
        }
    }

    @Around("execution(* top.ply.authservice.service.impl.WechatLoginWorkerImpl.sendToWechatServer(..))")
    public Object wechatServerLoginLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        if (logger.isInfoEnabled()) {
            logger.info("Sending js_code to wechat server: js_code = " + args[0]);
        }
        try {
            Object result = joinPoint.proceed();
            if (logger.isInfoEnabled()) {
                logger.info("Accept response from wechat server with js_code(" + args[0] + ") ", result);
            }
            return result;
        } catch (Throwable throwable) {
            logger.error("Error when send data to wechat server: {}", throwable.getStackTrace());
            throw throwable;
        }
    }

    @Around("execution(* top.ply.authservice.service.impl.WechatLoginWorkerImpl.saveWechatUser(..))")
    public Object saveWechatUserActionLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        if (logger.isInfoEnabled()) {
            logger.info("Save wechat user to database with parameters: ", args);
        }
        try {
            Object result = joinPoint.proceed();
            if (logger.isInfoEnabled()) {
                logger.info("Save wechat user to database success");
            }
            return result;
        } catch (Throwable throwable) {
            logger.error("Save wechat user(" + args[0] + ") to database error: {}", throwable.getStackTrace());
            throw throwable;
        }
    }

}
