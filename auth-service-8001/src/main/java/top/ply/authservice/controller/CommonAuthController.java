package top.ply.authservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.ply.authservice.service.impl.CommonAuthServiceFacade;
import top.ply.common_unit.entity.RespEntity;

import java.util.Map;

@RestController
@RequestMapping("/common")
public class CommonAuthController {

    @Autowired
    CommonAuthServiceFacade commonAuthService;

    /**
     * 使用密码进行登录接口，account参数支持Email、Telephone、Username登录
     * @param account 用户的身份信息，支持Email、Telephone、Username
     * @param password 用户密码，要求前端加密后传输
     * @return {code: 处理响应码, msg: 处理信息, data: {token: 处理成功后下发的token凭证}}
     */
    @PostMapping("/login/password")
    public RespEntity loginWithPassword(String account, String password) {
        return commonAuthService.loginWithPassword(account, password);
    }

    /**
     * 使用验证码登陆时，下发验证码的接口，address参数支持Email、Telephone
     * @param address 验证码下发地址，支持Telephone、Email
     * @return {code: 处理响应码, msg: 处理信息}
     */
    @GetMapping("/login/code")
    public RespEntity sendVerifyCode(String address) {
        return commonAuthService.sendVerifyCode(address);
    }

    /**
     * 使用验证码进行登录时，验证验证码的接口 address参数支持Email、Telephone、Username
     * @param address 验证码下发时的地址，支持Email，Telephone
     * @param code 下发的验证码
     * @return {code: 处理响应码, msg: 处理信息, data: {token: 处理成功后下发的token凭证}}
     */
    @PostMapping("/login/verify")
    public RespEntity verifySentCode(String address, String code) {
        return commonAuthService.verifySentCode(address, code);
    }

}
