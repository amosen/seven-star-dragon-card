package top.ply.authservice.dao;

import org.springframework.stereotype.Repository;
import top.ply.authservice.pojo.CommonUser;

@Repository
public interface CommonUserMapper {

    public CommonUser getUserByPrimaryKey(Integer id);

    public CommonUser getUserByID(String id);

    public CommonUser getUserByTelephone(String telephone);

    public CommonUser getUserByEmail(String email);

    public CommonUser getUserByUsername(String username);

    public int addCommonUser(CommonUser commonUser);

    public int deleteCommonUser(CommonUser commonUser);

    public int updateCommonUserStatus(CommonUser commonUser);

}
