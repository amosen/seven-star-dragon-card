package top.ply.authservice.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusMapper {

    public Integer getNormalStatusCode();

    public int updateNormalStatusCode(int statusCode);

}
