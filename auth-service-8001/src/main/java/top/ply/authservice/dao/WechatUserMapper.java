package top.ply.authservice.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import top.ply.authservice.pojo.WechatUser;

@Repository
public interface WechatUserMapper {

    public WechatUser getWechatUserByUserID(String userID);

    public WechatUser getWechatUserByPrimaryKey(long id);

    public WechatUser getWechatUserByOpenId(String openid);

    public int addWechatUser(WechatUser wechatUser);

    public int updateStatus(WechatUser wechatUser);

    public int deleteWechatUser(WechatUser wechatUser);
}
