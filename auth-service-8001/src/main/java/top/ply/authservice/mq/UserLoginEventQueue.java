package top.ply.authservice.mq;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

@Deprecated
public interface UserLoginEventQueue {

    static final String WECHAT_USER_LOGIN_EVENT_QUEUE_NAME = "wechat-user-login-event-queue";

    static final String COMMON_USER_LOGIN_EVENT_QUEUE_NAME = "common-user-login-event-queue";

    @Output(WECHAT_USER_LOGIN_EVENT_QUEUE_NAME)
    MessageChannel wechatUserLoginEventOutput();

    @Output(COMMON_USER_LOGIN_EVENT_QUEUE_NAME)
    MessageChannel commonUserLoginEventOutput();

}
