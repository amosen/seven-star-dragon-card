package top.ply.authservice.mq;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

@Deprecated
public interface SendVerifyCodeQueue {

    static final String MAIL_CODE_SENDING_QUEUE_NAME = "email-verify-code-queue";

    static final String PHONE_CODE_SENDING_QUEUE_NAME = "phone-verify-code-queue";

    @Output(MAIL_CODE_SENDING_QUEUE_NAME)
    MessageChannel sendMailCode();

    @Output(PHONE_CODE_SENDING_QUEUE_NAME)
    MessageChannel sendPhoneCode();

}
