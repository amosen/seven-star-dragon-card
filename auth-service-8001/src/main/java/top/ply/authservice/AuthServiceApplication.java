package top.ply.authservice;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import top.ply.authservice.mq.SendVerifyCodeQueue;
import top.ply.authservice.mq.UserLoginEventQueue;

@SpringBootApplication
@MapperScan(basePackages = {"top.ply.authservice.dao"})
@EnableBinding({UserLoginEventQueue.class, SendVerifyCodeQueue.class})
@EnableDubbo(scanBasePackages = {"top.ply.authservice.service"})
@ServletComponentScan
public class AuthServiceApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(AuthServiceApplication.class, args);
    }




}
