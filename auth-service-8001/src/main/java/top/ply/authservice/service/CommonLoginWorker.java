package top.ply.authservice.service;

import top.ply.common_unit.entity.RespEntity;

public interface CommonLoginWorker extends AuthService {

    boolean isNormal(String token);

    String commonLoginWithEmail(String email, String password);

    String commonLoginWithTelephone(String telephone, String password);

    String commonLoginWithAccount(String username, String password);

    RespEntity sendCodeWithEmail(String email);

    RespEntity sendCodeWithTelephone(String telephone);

    String verifyCodeWithTelephone(String telephone, String code);

    String verifyCodeWithEmail(String email, String code);

}
