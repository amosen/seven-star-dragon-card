package top.ply.authservice.service;

import top.ply.authservice.pojo.WechatUser;

public interface WechatLoginWorker extends AuthService {

    /**
     * 根据UserID判断账号是否正常
     * @param userID 用户的唯一标识
     * @return true表示为正常，false表示异常
     */
    boolean accountIsNormal(String userID);

    /**
     * 微信用户登录接口
     * @param js_code 微信小程序端生成的动态js码
     * @return 服务端下发token
     */
    String wechatUserLogin(String js_code);

}
