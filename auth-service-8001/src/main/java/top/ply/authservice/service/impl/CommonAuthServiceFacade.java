package top.ply.authservice.service.impl;

import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import top.ply.authservice.pojo.CommonUser;
import top.ply.authservice.service.AuthService;
import top.ply.authservice.service.CommonAuthService;
import top.ply.common_unit.entity.RespEntity;
import top.ply.common_unit.global_resp.RespUtil;

import java.util.HashMap;
import java.util.Map;

@Service
@DubboService(interfaceClass = CommonAuthService.class)
@RefreshScope
public class CommonAuthServiceFacade implements CommonAuthService {

    @Autowired
    private CommonLoginWorkerImpl commonLoginWorker;

    public RespEntity loginWithPassword(String account, String password) {
        if (CommonAuthService.PHONE_PATTERN.matcher(account).matches()) {
            String token = commonLoginWorker.commonLoginWithTelephone(account, password);
            return judgeAndReturn(token);
        } else if (CommonAuthService.EMAIL_PATTERN.matcher(account).matches()) {
            String token = commonLoginWorker.commonLoginWithEmail(account, password);
            return judgeAndReturn(token);
        } else {
            return judgeAndReturn(commonLoginWorker.commonLoginWithAccount(account, password));
        }
    }

    private RespEntity judgeAndReturn(String token) {
        if (token == null) {
            Integer errReason = commonLoginWorker.getErrReason();
            if (errReason.equals(CommonLoginWorkerImpl.ILLEGAL_PARAMETER_CODE)) {
                return RespUtil.illegalParameter();
            } else if (errReason.equals(CommonLoginWorkerImpl.WRONG_USER_STATE_CODE)) {
                return RespUtil.userStatusError();
            } else if (errReason.equals(CommonLoginWorkerImpl.WRONG_ACCOUNT_OR_PASSWORD)) {
                return RespUtil.userPasswordError();
            } else {
                return RespUtil.baseFail();
            }
        } else {
            Map<String, Object> resp = new HashMap<>();
            resp.put(CommonLoginWorkerImpl.TOKEN_KEY, token);
            return RespUtil.baseSuccess(resp);
        }
    }

    public RespEntity sendVerifyCode(String address) {
        if (CommonAuthService.PHONE_PATTERN.matcher(address).matches()) {
            return commonLoginWorker.sendCodeWithTelephone(address);
        } else if (CommonAuthService.EMAIL_PATTERN.matcher(address).matches()) {
            return commonLoginWorker.sendCodeWithEmail(address);
        }
        return RespUtil.illegalParameter();
    }

    public RespEntity verifySentCode(String address, String code) {
        if (CommonAuthService.PHONE_PATTERN.matcher(address).matches()) {
            String token = commonLoginWorker.verifyCodeWithTelephone(address, code);
            return judgeAndReturn(token);
        } else if (CommonAuthService.EMAIL_PATTERN.matcher(address).matches()) {
            String token = commonLoginWorker.verifyCodeWithEmail(address, code);
            return judgeAndReturn(token);
        } else {
            return RespUtil.illegalParameter();
        }
    }

    @Override
    public boolean verifyToken(String token) {
        return commonLoginWorker.verifyToken(token);
    }

    @Override
    public String getUserIDFromToken(String token) {
        return commonLoginWorker.getUserIDFromToken(token);
    }

    @Override
    public String refreshToken(String token) {
        return commonLoginWorker.refreshToken(token);
    }

    @Override
    public CommonUser getCommonUserInfo(String token) {
        return commonLoginWorker.getCommonUserInfo(token);
    }
}
